﻿#include <iostream>
#include <locale>

int findDividers(int num, int x);
int findDividersWrapper(int num, int x);

void findFriendlyNumbers(int start, int stop) {

    int num1 = 0;
    int num2 = 0;

    for (int i = start; i < stop; i++) {
        num1 = findDividersWrapper(i, 1);
        num2 = findDividersWrapper(num1, 1);
        if (num2 == i) {
            printf("Число %d имеет дружественное число %d\n", num1, num2);
        }
    }

}


int findDividersWrapper(int num, int x) {

    int helper = 0;
    int res = 0;

    if (num > 2500) {
        helper = num - 2500;
    }

    res += findDividers(helper, 1);
    res += findDividers(num - helper, 1);
    return res;

}

int findDividers(int num, int x) {
    static int cnt = 0;

    if (x > num) {
        //printf("cnt = %d\n", cnt);
        int ret = cnt;
        cnt = 0;
        return ret;
    }

    if (num % x == 0) {
        //printf("%d\n", x);
        if (x < num) {
            cnt += x;
        }
    }
    x++;
    findDividers(num, x);
}



int main()
{
    setlocale(LC_ALL, "Rus");
    findFriendlyNumbers(200, 3000);
    return 0;
}
